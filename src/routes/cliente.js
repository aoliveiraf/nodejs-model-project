const router = require("express").Router()
const db = require("../config/database")

const col_name = "cliente"

router.post("/",async (req,res)=>{

    const cliente = req.body;

    const result = await db.collection(col_name).save(cliente) 

    res.send(result)
})


router.get("/",(req,res)=>{

    const result = {
        "name":"Nome da pessoa",
        "id":"Id da pessoa"
    }
    res.send(result)
})

router.put("/",(req,res)=>{


    const result = {
        "name":"Nome da pessoa",
        "id":"Id da pessoa"
    }
    res.send(result)
})

module.exports = router